<?php

namespace Applistage\Config;

class Database { // SINGLETON PATTERN
    private static $bdd = null;

    private function __construct ()
    {

    }

    public static function getDatabase ()
    {
        if (is_null(self::$bdd)) {
            self::$bdd = new \PDO(
                "pgsql:host=db;port=5432;dbname=applistage_db;options='--client_encoding=UTF8'",
                "applistage_user",
                "sOzMIF7XLU"
            );
        }
        return self::$bdd;
    }
}