<?php

namespace Applistage\Config;

use Applistage\Controller\AuthController;
use Applistage\Controller\HomeController;
use Applistage\Controller\ImageController;
use Applistage\Controller\StaticController;
use Applistage\Controller\TaskController;

class Routes {
    static private array $routes = [
        '/' => [HomeController::class, 'index'], // [class, 'function']

        '/login' => [AuthController::class, 'login'],
        '/register' => [AuthController::class, 'register'],
        '/logout' => [AuthController::class, 'logout'],

        '/tasks' => [TaskController::class, 'listTasks'],
        '/tasks/show/:id' => [TaskController::class, 'showTask'],
        '/tasks/new' => [TaskController::class, 'newTask'],
        '/tasks/edit/:id' => [TaskController::class, 'editTask'],
        '/tasks/delete/:id' => [TaskController::class, 'deleteTask'],
        '/test/:slug-:id-:date' => [TaskController::class, 'test'],

        '/images' => [ImageController::class, 'listImages'],
        '/images/new' => [ImageController::class, 'newImage'],
        '/images/show/:id' => [ImageController::class, 'showImage'],

        '/assets/dist/:file' => [StaticController::class, 'serveAsset'],
        '/medias/images/:file' => [StaticController::class, 'serveImage'],
        '/medias/thumbnails/:file' => [StaticController::class, 'serveThumbnail'],

        '/403' => [HomeController::class, 'error403'],
        '/404' => [HomeController::class, 'error404'],
    ];

    public static function getRoutes () : array
    {
        return self::$routes;
    }
}

