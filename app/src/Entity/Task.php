<?php

namespace Applistage\Entity;

class Task {
    private int $id;
    private string $title;

    public function __construct ($id=-1, $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    public function getId () : int
    {
        return $this->id;
    }

    public function getTitle () : string
    {
        return $this->title;
    }

    public function setTitle($title) : Task
    {
        $this->title = $title;
        return $this;
    }
}