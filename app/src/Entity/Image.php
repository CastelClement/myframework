<?php

namespace Applistage\Entity;

class Image {
    private int $id;
    private string $filename;
    private int $filesize;

    public function __construct($id=-1, $filename, $filesize)
    {
        $this->id = $id;
        $this->filename = $filename;
        $this->filesize = $filesize;
    }

    public function getId () : int
    {
        return $this->id;
    }

    public function getFilename () : string
    {
        return $this->filename;
    }

    public function setFilename($filename) : Image
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFilesize () : int
    {
        return $this->filesize;
    }

    public function setFilesize($filesize) : Image
    {
        $this->filesize = $filesize;
        return $this;
    }
}