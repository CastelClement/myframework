<?php

namespace Applistage\Entity;

class User {
    private int $id;
    private string $email;
    private string $password;
    private string $role;

    public function __construct ($id=-1, $email, $password, $role)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
    }

    public function getId () : int
    {
        return $this->id;
    }

    public function getEmail () : string
    {
        return $this->email;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setEmail (string $email) : User
    {
        $this->email = $email;
        return $this;
    }

    public function setPassword (string $password) : User
    {
        $this->password = $password;
        return $this;
    }

    public function getRole () : string
    {
        return $this->role;
    }
}