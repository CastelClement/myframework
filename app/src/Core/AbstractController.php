<?php

namespace Applistage\Core;

use Applistage\Controller\HomeController;
use Applistage\Entity\User;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class AbstractController
{
    protected $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader(dirname(dirname(__DIR__)) . '/templates');
        $twig = new Environment($loader, [
            // 'cache' => '/var/www/cache',
            'cache' => false,
        ]);
        $this->twig = $twig;
    }

    protected function render ($templatePath, $params=[])
    {
        $template = $this->twig->load($templatePath);
        echo $template->render($params);
    }

    protected function setHttpResponseCode ($httpResponseCode)
    {
        http_response_code($httpResponseCode);
    }

    protected function setContentMimeType ($mime)
    {
        header('Content-Type: ' . $mime);
    }

    protected function redirectTo ($url)
    {
        header('Location: ' . $url);
    }

    protected function needRole (array $allowedRoles)
    {
        if (UserHelper::isLoggedIn()) {
            if (!in_array(UserHelper::getRole(), $allowedRoles)) {
                $this->redirectTo('/403');
            }
        } else {
            $this->redirectTo("/login");
        }
    }
}