<?php

namespace Applistage\Core;

use Applistage\Config\Routes;

class Router {
    // method listening for the http request
    static public function parse(Request $request)
    {
        $uri = $request->getUri();

        return self::match_and_exec($uri);
    }

    // Search for a matching route and execute the related function
    static private function match_and_exec($uri)
    {
        $possible_routes = Routes::getRoutes();
        foreach($possible_routes as $route => $callback)
        {
            $result = self::match($route, $uri);
            if (is_array($result) == false)
            {
                continue;
            }
            return [
                'callback' => $callback,
                'params' => $result,
            ];
        }
        return false;
    }

    static private function match($route, $uri)
    {
        // replacing variables by a variable regexp
        $regexp_path = preg_replace ('#:([\w|.]+)#' , '([a-zA-Z0-9.]+)' , $route);
        // print("\nInitial path " . $this->path);
        // print("\npath after filtering: " . $regexp_path);

        // searching for variables in the given path
        $result = preg_match('#^'.$regexp_path.'$#', $uri, $matches);
        // print("\n>" . $regexp_path . "< >" . $path . "< match result : " . $result);
        // print("\n");

        // removing the first value of the $matches cause it's not a match.
        array_splice($matches, 0, 1);

        if ($result == 1)
        {
            return $matches;
        }
        return false;
    }
}