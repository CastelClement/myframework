<?php

namespace Applistage\Core;

abstract class AbstractRepository
{
    protected static function hydrate($arr, $class) : array
    {
        $ret = [];
        $class = new \ReflectionClass($class);
        foreach ($arr as $row) {
            $instance = $class->newInstanceArgs($row);
            array_push($ret, $instance);
        }
        return $ret;
    }
}