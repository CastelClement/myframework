<?php

namespace Applistage\Core;

class UserHelper {
    public static function isLoggedIn () : bool
    {
        return isset($_SESSION['sec_role']);
    }

    public static function getRole () : string
    {
        return $_SESSION['sec_role'];
    }

    public static function setRole(string $role)
    {
        $_SESSION['sec_role'] = $role;
    }

    public static function logout ()
    {
        unset($_SESSION['sec_role']);
        session_destroy();
    }
}