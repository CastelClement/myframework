<?php

namespace Applistage\Core;

class Dispatcher {
    public function dispatch ()
    {
        $request = Request::fromGlobals();

        $ret = Router::parse($request);

        if (!$ret) { // ERROR : ROUTE NOT FOUND
            $request = new Request('/404');
            $ret = Router::parse($request);
        }

        $controller = new $ret['callback'][0]();

        call_user_func_array(array($controller, $ret['callback'][1]), $ret['params']);
    }
}