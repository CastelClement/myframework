<?php

namespace Applistage\Core;

class Request
{
    private $uri;
    private $httpMethod;

    public function __construct(
        string $uri = '/',
        string $httpMethod = 'GET'
    )
    {
        $this->uri = $uri;
        $this->httpMethod = $httpMethod;
    }

    public static function fromGlobals() : Request
    {
        $uri = $_SERVER["REQUEST_URI"];
        $method = $_SERVER["REQUEST_METHOD"];

        return new Request(
            $uri,
            $method,
        );
    }

    public function getUri() : string
    {
        return $this->uri;
    }

    public function getMethod () : string
    {
        return $this->httpMethod;
    }
}