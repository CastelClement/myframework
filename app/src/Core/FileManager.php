<?php

namespace Applistage\Core;

class FileManager
{
    public static string $BASE_DIR;

    public function __construct()
    {
        FileManager::$BASE_DIR = dirname(dirname(__DIR__)) . '/static/';
    }


    public static function persist ($filename, $directory)
    {
        $array = explode('.', $filename["name"]);
        $extension = end($array);
        $filenameLocal = self::$BASE_DIR . 'medias/' . $directory . basename($filename["tmp_name"]) . '.' . $extension;

        move_uploaded_file($filename["tmp_name"], $filenameLocal);

        return '/medias/' . $directory . basename($filename["tmp_name"]) . '.' . $extension; // filename (public url)
    }

    /**
     * @param $file file's public url
     * @return false|int
     */
    public static function getSize ($filepath)
    {
        return filesize(self::$BASE_DIR . $filepath);
    }

    public static function publicPathToLocal($filepath)
    {
        return self::$BASE_DIR . substr($filepath, 1);
    }

    public static function getFilename ($filepath)
    {
        $array = explode('/', $filepath);
        return explode('.', end($array))[0];
    }
}