<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;
use Applistage\Core\Request;
use Applistage\Core\UserHelper;
use Applistage\Entity\User;
use Applistage\Repository\UserRepository;

class AuthController extends AbstractController
{
    public function login ()
    {
        $request = Request::fromGlobals();
        if ($request->getMethod() == 'GET') {
            $this->render('auth/login.html.twig');
        } else if ($request->getMethod() == 'POST') {
            $user = UserRepository::findByEmail($_POST['email']);
            if ($user) {
                if(password_verify($_POST['password'], $user->getPassword())) {
                    UserHelper::setRole($user->getRole());
                    dd('role d2 : ', UserHelper::getRole());
                } else {
                    dd("invalid password");
                }
            } else {
                dd("email not found");
            }
        }
    }

    public function register ()
    {
        $request = Request::fromGlobals();
        if ($request->getMethod() == 'GET') {
            $this->render('auth/register.html.twig');
        } else if ($request->getMethod() == 'POST') {
            $passwordHash = password_hash($_POST['password'], PASSWORD_ARGON2ID);

            $user = new User();
            $user
                ->setEmail($_POST['email'])
                ->setPassword($passwordHash);

            UserRepository::add($user);

            $this->redirectTo('/login');
        }
    }

    public function logout ()
    {
        UserHelper::logout();

        $this->redirectTo("/");
    }
}