<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;
use Applistage\Core\FileManager;

class StaticController extends AbstractController
{
    public function serveAsset (string $file)
    {
        $filePath = FileManager::$BASE_DIR . 'assets/dist/' . $file;
        $array = explode('.', $filePath);
        $extension = end($array);

        if ($extension === 'css') {
            $this->setContentMimeType('text/css');
        } else if ($extension == 'js') {
            $this->setContentMimeType('application/javascript');
        }

        readfile($filePath);
    }

    public function serveImage (string $file)
    {
        $this->needRole(['ADMIN']);

        $filePath = FileManager::$BASE_DIR . 'medias/images/' . $file;
        $this->setContentMimeType(mime_content_type($filePath));

        readfile($filePath);
    }

    public function serveThumbnail (string $file)
    {
        $this->needRole(['ADMIN']);

        $filePath = FileManager::$BASE_DIR . 'medias/thumbnails/' . $file;

        $imagick = new \Imagick($filePath);

        $this->setContentMimeType(mime_content_type($filePath));

        $imagick->scaleImage(315, 445, true); // A4 format
        echo $imagick->getImageBlob();
    }
}