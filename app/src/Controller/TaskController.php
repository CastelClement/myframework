<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;
use Applistage\Core\Request;
use Applistage\Entity\Task;
use Applistage\Repository\TaskRepository;

class TaskController extends AbstractController
{
    public function listTasks ()
    {
        $tasks = TaskRepository::fetchAll();

        $this->render('tasks/listing.html.twig', ['tasks' => $tasks]);
    }

    public function showTask ($params)
    {
        $task = TaskRepository::findById((int)$params);

        if ($task) { // task found in DB
            $this->render('tasks/show.html.twig', ['task' => $task]);
        } else {
            dd("404 : Task not found");
        }
    }

    public function newTask ()
    {
        $request = Request::fromGlobals();
        if ($request->getMethod() == 'GET') {
            $this->render('tasks/new.html.twig');
        } else if ($request->getMethod() == 'POST') {

            if ($_POST['submit'] && $_POST['title']) {
                if (!empty($_POST['title'])) {
                    // new Entity
                    $task = new Task();
                    $task->setTitle($_POST['title']);

                    // add Entity to repository & flush
                    TaskRepository::add($task);

                    // redirect to listing
                    $this->redirectTo('/tasks');
                }
            }
        }
    }

    public function editTask ($id)
    {
        $request = Request::fromGlobals();
        if ($request->getMethod() == 'GET') {
            $task = TaskRepository::findById($id);
            $this->render('tasks/edit.html.twig', ['task' => $task]);
        } else if ($request->getMethod() == 'POST') {
            if ($_POST['submit'] && $_POST['title']) {
                if (!empty($_POST['title'])) {
                    $task = TaskRepository::findById($id);
                    $task->setTitle($_POST['title']);

                    TaskRepository::update($task);

                    $this->redirectTo('/tasks');
                }
            }
        }
    }

    public function deleteTask ($id)
    {
        TaskRepository::delete($id);
        $this->redirectTo('/tasks');
    }

    public function test ($slug, $id, $date)
    {
        dd('slug : ' . $slug, 'id : ' . $id, 'date : ' . $date);
    }
}
