<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;

class HomeController extends AbstractController
{
    public function index ()
    {
        $this->render('home.html.twig');
    }

    public function error403 ()
    {
        $this->setHttpResponseCode(403);

        $this->render('errors/error403.html.twig');
    }

    public function error404 ()
    {
        $this->setHttpResponseCode(404);

        $this->render('errors/error404.html.twig');
    }
}