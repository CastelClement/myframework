<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;
use Applistage\Core\FileManager;
use Applistage\Core\Request;
use Applistage\Core\UserHelper;
use Applistage\Entity\Image;
use Applistage\Repository\ImageRepository;

class ImageController extends AbstractController
{
    public function listImages()
    {
        $this->needRole(['DEFAULT', 'ADMIN']);

        $images = ImageRepository::fetchAll();

        $this->render('images/listing.html.twig', ['images' => $images]);
    }

    public function newImage ()
    {
        $this->needRole(['ADMIN']);

        $request = Request::fromGlobals();
        if ($request->getMethod() == 'GET') {
            $this->render('images/new.html.twig');
        } else if ($request->getMethod() == 'POST') {

            if ($_POST['submit']) {
                // new Entity
                $publicUrl = FileManager::persist($_FILES["image"], "images/");

                $imagick = new \Imagick();
                $imagick->setResolution(200, 200);
                $imagick->setSize('210', '297'); // A4 format
                $imagick->setFormat('png');
                $imagick->readImage(FileManager::publicPathToLocal($publicUrl));
                $thumbnailUrl = '/medias/thumbnails/' . FileManager::getFilename($publicUrl) . '.png';
                $imagick->writeImage(FileManager::publicPathToLocal($thumbnailUrl));

                $image = new Image();
                $image
                    ->setFilename($thumbnailUrl)
                    ->setFilesize(FileManager::getSize($thumbnailUrl));

                // add Entity to repository & flush
                ImageRepository::add($image);

                // redirect to listing
                $this->redirectTo('/images');
            }
        }
    }

    public function showImage ($params)
    {
        $this->needRole(['ADMIN']);

        $image = ImageRepository::findById((int)$params);

        if ($image) { // image found in DB
            $this->render('images/show.html.twig', ['image' => $image]);
        } else {
            dd("404 : Image not found");
        }
    }
}