<?php

namespace Applistage\Repository;

use Applistage\Config\Database;
use Applistage\Core\AbstractRepository;
use Applistage\Entity\User;


class UserRepository extends AbstractRepository
{
    protected static function hydrate ($arr, $class=User::class) : array
    {
        return parent::hydrate($arr, $class);
    }

    public static function findByEmail(string $email) : User
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM \"Users\" WHERE email=:email;";
        $query = $db->prepare($sql);
        $query->execute([
            ':email' => $email,
        ]);

        return $query->fetchAll(\PDO::FETCH_CLASS, User::class)[0];
    }

    public static function add(User $user)
    {
        $db = Database::getDatabase();

        $sql = "INSERT INTO \"Users\" (email, password, role) VALUES (:email, :password, :role);";
        $query = $db->prepare($sql);
        $query->execute([
            ':email' => $user->getEmail(),
            ':password' => $user->getPassword(),
            ':role' => 'DEFAULT'
        ]);
    }
}