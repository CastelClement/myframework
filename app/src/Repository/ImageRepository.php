<?php

namespace Applistage\Repository;

use Applistage\Config\Database;
use Applistage\Core\AbstractRepository;
use Applistage\Entity\Image;


class ImageRepository extends AbstractRepository
{
    protected static function hydrate ($arr, $class=Image::class) : array
    {
        return parent::hydrate($arr, $class);
    }

    public static function fetchAll ()
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM Images;";
        $query = $db->prepare($sql);
        $query->execute();

        return self::hydrate($query->fetchAll(\PDO::FETCH_ASSOC));
    }

    public static function add(Image $image)
    {
        $db = Database::getDatabase();

        $sql = "INSERT INTO Images (filename, filesize) VALUES (:filename, :filesize);";
        $query = $db->prepare($sql);
        $query->execute([
            ':filename' => $image->getFilename(),
            ':filesize' => $image->getFilesize(),
        ]);
    }

    public static function findById($id) : Image
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM Images WHERE id=:id;";
        $query = $db->prepare($sql);
        $query->execute([
            ':id' => $id,
        ]);

        return self::hydrate($query->fetchAll(\PDO::FETCH_ASSOC))[0];
    }
}