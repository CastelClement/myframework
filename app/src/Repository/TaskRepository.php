<?php

namespace Applistage\Repository;

use Applistage\Config\Database;
use Applistage\Core\AbstractRepository;
use Applistage\Entity\Task;

class TaskRepository extends AbstractRepository
{
    protected static function hydrate ($arr, $class=Task::class) : array
    {
        return parent::hydrate($arr, $class);
    }

    public static function fetchAll ()
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM Tasks;";
        $query = $db->prepare($sql);
        $query->execute();

        return self::hydrate($query->fetchAll(\PDO::FETCH_ASSOC));
    }

    public static function findById($id) : Task
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM Tasks WHERE id=:id;";
        $query = $db->prepare($sql);
        $query->execute([
            ':id' => $id,
        ]);

        return self::hydrate($query->fetchAll(\PDO::FETCH_ASSOC))[0];
    }

    public static function add(Task $task)
    {
        $db = Database::getDatabase();

        $sql = "INSERT INTO Tasks (title) VALUES (:title);";
        $query = $db->prepare($sql);
        $query->execute([
            ':title' => $task->getTitle(),
        ]);
    }

    public static function update(Task $task)
    {
        $db = Database::getDatabase();

        $sql = "UPDATE Tasks SET title=:title WHERE id=:id;";
        $query = $db->prepare($sql);
        $query->execute([
            ':id' => $task->getId(),
            ':title' => $task->getTitle(),
        ]);
    }

    public static function delete ($id)
    {
        $db = Database::getDatabase();

        $sql = "DELETE FROM Tasks WHERE id=:id;";
        $query = $db->prepare($sql);
        $query->execute([
            ':id' => $id,
        ]);
    }
}