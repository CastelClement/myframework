import "./styles/index.scss";


const sidebarOpenIcon = document.getElementById("sidebar-icon-open");
const sidebarCloseIcon = document.getElementById("sidebar-icon-close");

sidebarOpenIcon.onclick = () => {
    document.getElementById("sidebar-menus").style.width = "fit-content";
    document.getElementById("sidebar-menus").style.opacity = "1";
    sidebarCloseIcon.style.display = "block";
    sidebarOpenIcon.style.display = "none";
}

sidebarCloseIcon.onclick = () => {
    document.getElementById("sidebar-menus").style.opacity = "0";
    sidebarOpenIcon.style.display = "block";
    sidebarCloseIcon.style.display = "none";
    setTimeout(() => {
        document.getElementById("sidebar-menus").style.width = "0";
    }, 500)
}