# APPLISTAGE

## TODO
* MODÉLISATION BDD
  * Algèbre relationnel (tables, types, contraintes, relations, triggers ligne, triggers table)
    * Modélisation UML ?
  * Intégration : scripts de création/destruction des tables/séquences/contraintes **[compatible avec Adminer si possible]**
  * Fixtures (script d'insertion de fausses données pour du test) **[utiliser la lib faker ?]**
  * Normaliser les données **[par exemple : quelles sont les constantes qu'on utilise pour les roles ?]**
* DESIGN BACK END
  * Lister toutes les routes (url et paramètres), ce qu'elles font & permissions **[Séparer le back et le front]**
* DESIGN FRONT END
  * Lister les pages à faire
  * Design des pages
  * Intégration : SCSS/HTML/JS


## Liens utiles :

| Lien | Description |
|---|---------------------------------------------------------|
| [Doc. Front End](./documentation/front_end.md) | Ensemble de la documentation du développement front end |
| [Doc. Back End](./documentation/back_end.md) | Ensemble de la documentation du développement back end |
| [Doc. Développement](./documentation/development.md) | Ensemble de commandes utiles pendant le développement |
| [Doc. Structure Projet](./documentation/structure_projet.md) | Description des fichiers/dossiers du dépot |
|  |  |
| [Figma](https://www.figma.com/file/5Goz3yjWBPawMyBbJH9xo1/Appli-Stage-%26-Soutenance?node-id=0%3A1) | Maquettes |
|  |  |
|  |  |
|  |  |




