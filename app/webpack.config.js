const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const path = require('path');

let mode = "development"
let target = "web"

if (process.env.NODE_ENV === "production") {
    mode = "production"
    target = "browserslist"
}


module.exports = {
    mode: mode,
    entry: path.resolve(__dirname, 'assets/index.js'),
    output: {
        path: path.resolve(__dirname, 'static/assets/dist/'),
    },

    module: {
        rules: [
            {
                test: /\.(sc|c)ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    "css-loader", 
                    "sass-loader"
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use:    {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.woff2$/,
                use: ['file-loader']
            }
        ],
    },

    plugins: [
        new MiniCssExtractPlugin(),
    ],
    
    target: target,

    devtool: "source-map",
    devServer: {
        contentBase: "./dist",
        hot: true
    },
}