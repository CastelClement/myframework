# Structure dépot

```shell
.
├── assets              # ensemble des fichiers sources SCSS/JS
│   ├── index.js        # Point d'entrée webpack
│   └── styles          # Ensemble des feuilles de style
├── cache               # Cache (uniquement utilisé par Twig pour le moment)
├── docker              # Images Docker personnalisées
├── documentation       # Documentation technique
├── public              # Point d'entrée du serveur PHP
│   ├── .htaccess       # Fichier de configuration du serveur Apache (règle de réécriture des urls)
│   └── index.php       # Point d'entrée du serveur PHP
├── src                 # Code Source
│   ├── Config          # Configuration : routes & BDD
│   ├── Controller      # Controlleurs d'application
│   ├── Core            # Fichiers utilitaires et classes abstraites : coeur du Framework
│   ├── Entity          # Ensemble des entités
│   ├── Repository      # Ensemble des Repository // tables de BDD
├── static              # Fichiers accessibles depuis le serveur web
│   ├── assets          # Fichiers CSS/JS compilés et optimisés
│   └── medias          # medias images, pdfs, ...
├── templates           # Ensemble des templates rendues par l'application

├── babel.config.js     # Configuration de Babel
├── .browserslistrc     # Règles de support des navigateurs
├── composer.json       # Dépendances PHP et configuration autoloader
├── docker-compose.yml  # Architecture de la stack technique : PHP, Postgres, Adminer, Nginx
├── package.json        # Dépendances JS
├── README.md           # Fichier README principal
├── site.conf           # Configuration du serveur Nginx
└── webpack.config.js   # Configuration de Webpack
```