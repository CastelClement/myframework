# Documentation Front End

## Structure
### Fichiers sources SCSS/JS
Dossier : [/assets/](/assets/)

*Guide SCSS (anglais) : https://www.youtube.com/watch?v=akDIJa0AP5c*


### Fichiers compilés CSS/JS
CSS : [/static/assets/dist/main.css](/static/assets/dist/main.css)

JS: [/static/assets/dist/main.js](/static/assets/dist/main.js)

## Commandes
### Webpack : Lancer le serveur de développement
```shell
npm run watch
```

### Webpack : Compiler les sources pour la prod
```shell
npm run build
```


# Guide Twig
Voici quelques règles intéressantes de twig :

| Code | Explication |
|---|---------------------------------------------------------|
| `{{ v }}` | Afficher le contenu de la variable v |
| `{% extends "base.html.twig" %}` | Étendre une autre template twig *(à placer en début de fichier)*  |
| `{% block mon_block %} ... {% endblock %}` | Définition d'un bloc dont le contenu pourra être remplacé dans les templates qui étendent le fichier courant |
| `{% for v in vArr %} ... {{ v.name }} ... {% endfor %}` | boucle foreach |
