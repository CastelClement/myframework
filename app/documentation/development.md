# Documentation Développement

## Docker-compose

### Lancer le serveur
```shell
sudo docker-compose up
```

### Exécuter une commande sur un container
```shell
sudo docker-compose exec <<CONTAINER_NAME>> <<COMMAND>>
```
par exemple :
```shell
sudo docker-compose exec php composer require --dev symfony/var-dumper
```


## NPM / Webpack

### NPM : Installer les dépendances
```shell
npm install
```

### Webpack : Lancer le serveur de développement
```shell
npm run watch
```

### Webpack : Compiler les sources pour la prod
```shell
npm run build
```

## Création de l'archive de Mise en production
**N.B. :** Bien penser à build les fichiers de style/script avec npm en local
```shell
tar --exclude='public/.htaccess' --exclude='src/Config/Database.php' --exclude='*.gitignore' --exclude='static/medias/images/*' --exclude='static/medias/thumbnails/*' public src static templates composer.json -zcvf <<nom_archive>>.tgz
```