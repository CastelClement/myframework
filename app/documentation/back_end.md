# Documentation Back End

## Créer une nouvelle route de A à Z

### Ajouter la route désirée à la configuration

Éditer le fichier [/src/Config/Routes.php](/src/Config/Routes.php) et ajouter une ligne dans le tableau `$routes` sous la forme :
```php
'/aze/qsd/wxc/:param1-:param2-:param3' => [MonControlleur::class, 'methodeAAppeler'],
```
Vous pouvez mettre l'url que vous voulez. Pour définir des paramètres, utiliser `:nom_du_parametre`.
Lorsque l'utilisateur accèdera à l'url définie, la méthode `methodeAAppeler` sera appelée sur la classe `MonControlleur`.

### Création du controlleur

Il faut maintenant créer la méthode `methodeAAppeler` et le controlleur `MonControlleur`.

La structure de base d'un controlleur avec cette unique méthode ressemble à ça :
```php
<?php

namespace Applistage\Controller;

use Applistage\Core\AbstractController;

class MonControlleur extends AbstractController
{
    public function methodeAAppeler ($param1, $param2, $param3)
    {
        // do something
    }
}
```

La signature de la méthode dépend des paramètres définis dans la route. 
Le nom de ces paramètres n'est pas important. Sachez juste que les paramètres seront transmis dans l'ordre dans lequel ils ont été définis.

Par exemple si j'accède à l'adresse : `/aze/qsd/wxc/12C-4H5-UI9`, nous aurons cet appel de méthode : `methodeAAppeler ('12C', '4H5', 'UI9');`.

### Rendu d'une vue avec passage d'information

#### Création du fichier template

Il faut d'abord créer un fichier template dans le dossier [templates/](templates/).

Par faire simple, prenons cet exemple de template ([Guide Twig](/documentation/front_end.md)):

```
{% extends "base.html.twig" %}

{% block body %}
    {{ ma_variable }}
{% endblock %}
```


#### Rendu depuis le controlleur

Depuis le controlleur il suffit de faire appel à la méthode `render(fichier_template, [k => v, k2 => v2])` de `Applistage\Core\AbstractController` pour rendre la template en lui passant des informations.

```php
public function methodeAAppeler ($param1, $param2, $param3)
    {
        $variable = 141;
        $this->render('mon_template.html.twig', ['ma_variable' => $variable]);
    }
```

Et la page web affiche bien `141` quand on accède à l'adresse donnée plus tôt.



## Récupérer des données depuis la base de données (proprement)

Avec le Framework vient une logique de ORM (Object-Relational Mapper). Cette logique permet de découpler les controlleurs (logique de l'application) de la base de données de l'application. Ce découplage respecte les contraintes du pattern MVC.

Nous avons deux dossiers importants pour les données : 
- [/src/Entity](/src/Entity) : contient l'ensemble des entités (objets)
- [/src/Repository](/src/Repository) : contient l'ensemble des mappers (// tables)

Pour récupérer des objets de la BDD depuis un controlleur, il faut faire appel au Repository de ces objets.

Par exemple pour l'entité `Task`, si je veux récupérer une tâche suivant son id depuis un controlleur :

Dans le controlleur :

```php
$task = TaskRepository::findById(846); // récupère un objet de type Task avec l'id 846 ou null
```

Dans le `TaskRepository` :

```php
<?php

namespace Applistage\Repository;

use Applistage\Config\Database;
use Applistage\Core\AbstractRepository;
use Applistage\Entity\Task;

class TaskRepository extends AbstractRepository
{
    protected static function hydrate ($arr, $class=Task::class) : array
    {
        return parent::hydrate($arr, $class);
    }

    public static function findById($id) : Task
    {
        $db = Database::getDatabase();

        $sql = "SELECT * FROM Tasks WHERE id=:id;";
        $query = $db->prepare($sql);
        $query->execute([
            ':id' => $id,
        ]);

        return self::hydrate($query->fetchAll(\PDO::FETCH_ASSOC))[0];
    }
}
```

**N.B. : La fonction hydrate utilise la généricité. Il suffit de la mettre une seule fois par classe et modifier la valeur par défaut du paramètre $class pour qu'elle fonctionne.**

Objet `Task` :

```php
<?php

namespace Applistage\Entity;

class Task {
    private int $id;
    private string $title;

    public function __construct ($id=-1, $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    public function getId () : int
    {
        return $this->id;
    }

    public function getTitle () : string
    {
        return $this->title;
    }
}
```