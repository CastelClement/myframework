<?php
use Applistage\Core\Dispatcher;

require dirname(__DIR__).'/vendor/autoload.php';

session_start();

// >>> DEV ONLY
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();
// <<< DEV ONLY


$fileManager = new \Applistage\Core\FileManager(); // init BASE_DIR static variable

$dispatcher = new Dispatcher();
$dispatcher->dispatch();