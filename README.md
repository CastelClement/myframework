# Appli Stage et soutenance

GDOc : [https://docs.google.com/document/d/1PsSUdCCkcy4C7LkWtzNI7An0FmpRyJb4EXlZ9-J9AnE/edit?usp=sharing]()


## Tasks :
- Mock up graphical interfaces (+ Figma in the future)
- Define the database structure|models
- Define some use cases/EPIC stories
- Define main (+ secondary) functions
- Tasks distribution